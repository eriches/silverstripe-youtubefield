<?php
namespace Hestec\YoutubeField\ORM\FieldType;
use SilverStripe\ORM\FieldType\DBComposite;
use SilverStripe\ORM\FieldType\DBVarchar;
use SilverStripe\ORM\ArrayList;
use SilverStripe\View\ArrayData;
/**
 * A DB field that serialises an array before writing it to the db, and returning the array
 * back to the end user.
 *
 * @author Marcus Nyeholt <marcus@symbiote.com.au>
 */
class DBYoutube extends DBVarchar
{
    /**
     * @param array
     */
    public function __construct($name = null, $size = 255)
    {
        parent::__construct($name, $size);
    }

    public function getCode(){

        $url = $this->value;

        preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match);
        $output = $match[1];

        return $output;

    }

}